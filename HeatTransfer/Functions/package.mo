within TAeZoSysPro_testsuite.HeatTransfer;

package Functions

  extends Modelica.Icons.Package ;

  annotation(
    Icon(graphics = {Bitmap(extent = {{-80, 80}, {80, -80}}, fileName = "modelica://TAeZoSysPro_testsuite/HeatTransfer/Functions/image_package.png")}, coordinateSystem(initialScale = 0.1)));

end Functions;
